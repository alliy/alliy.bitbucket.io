function getWeather(){

	// get user input and request actual address
	var loc = document.getElementById("loc")
	var location = new XMLHttpRequest();
	location.open("GET", "https://maps.googleapis.com/maps/api/geocode/json?address=" + loc.value + "&key=AIzaSyAq6Y5zwTxNz47FBk4EXz7s9B7bf23fLLI", false);
	location.send();

	location.onreadystatechange = parseLocation(location);
}


function parseLocation(loc){

	// convert location to latitude and longitude
	var parsed = JSON.parse(loc.response);
	var lat = parsed.results[0].geometry.location.lat;
	var lng = parsed.results[0].geometry.location.lng;

	// get relevant weather details
	getForecast(lat, lng, parsed);
}

function saveUserData(parsed){

	// get user's IP address
	var userIP;
	var x = [];

	$.getJSON("https://jsonip.com/?callback=?", function(data){

		userIP = data.ip;

		// convert to list and add to it
		var d = JSON.parse(localStorage.getItem(userIP));
		if (d)
			x = d.concat(parsed.results[0].formatted_address);
		else
			x.push(parsed.results[0].formatted_address)

		// convert back to string and save it
		d = JSON.stringify(x);
		window.localStorage.setItem(userIP, d);

		for (var i = 1; i < 6; i++){

			if (x[i])
				document.getElementById("loc-" + i).innerHTML = x[i];
		}

	})

}

function getForecast(lat, lng, parsed){

	// save user data for later use
	saveUserData(parsed);

	// request weather
	var weather = new XMLHttpRequest();
	weather.open("GET", "https://api.darksky.net/forecast/5319a699f1d440da24185b3611ab1c89/" + lat + "," + lng, false);
	weather.send();

	// get current UTC
	var date = new Date();
	var currentUTC = Math.round(date.getTime()/1000);

	// parse out this week's forecast for weather
	var parsedWeather = JSON.parse(weather.response);
	var weekTemps = [];
	var weekDays = [];
	var weekHumidities = [];
	var precipProb = [];
	var humidity = [];
	var wind = [];

	// parse out today's weather forecast, set date and summary
	var currentSummary = "Today's forecast is " + parsedWeather.currently.summary.toLowerCase() + " with a temperature of " + parsedWeather.currently.temperature + " degrees fahrenheit.";
	document.getElementById("today-date").innerHTML = date.getMonth() + "/" + date.getDate() + " for " + parsed.results[0].formatted_address;
	document.getElementById("today-temp").innerHTML = currentSummary;

	// set weather icon
	var sky = new Skycons();
	sky.add(document.getElementById("weather-icon"), parsedWeather.currently.icon);
	sky.play();

	// get this week's weather info
	for (var i = 1; i < parsedWeather.daily.data.length; i++){
		weekTemps.push(parsedWeather.daily.data[i].temperatureMax + "F");
		weekDays.push(date.getMonth() + "/" + (date.getDate() + i));
		precipProb.push(Math.round(parsedWeather.daily.data[i].precipProbability*100) + "%");
		humidity.push(Math.round(parsedWeather.daily.data[i].humidity*100) + "%");
		wind.push(parsedWeather.daily.data[i].windSpeed + " MPH");
	}

	// set up weekdays in table
	for (var i = 1; i <= 7; i++){
		document.getElementById("day-" + i).innerHTML = weekDays[i-1];
		document.getElementById("temp-" + i).innerHTML = weekTemps[i-1];
		document.getElementById("precip-" + i).innerHTML = precipProb[i-1];
		document.getElementById("humid-" + i).innerHTML = humidity[i-1];
		document.getElementById("wind-" + i).innerHTML = wind[i-1];
	}

	// reveal tables and headers
	document.getElementById("history-header").style.visibility = "visible";
	$("table").animate({
		opacity: '1'
	});

	$("forecast-header").animate({
		opacity: '1'
	});

	// HISTORY
	// save number of seconds in different times units
	var SEC_IN_DAY = 86400;
	var SEC_IN_WEEK = 604800;
	var SEC_IN_MONTH = 2419200;
	var SEC_IN_YEAR = 31622400;

	// save labels and dates
	var historyLabels = ["Yesterday", "1 Week Ago", "1 Month Ago", "6 Months Ago", "1 Year Ago", "5 Years Ago"];
	var dates = [SEC_IN_DAY, SEC_IN_WEEK, SEC_IN_MONTH, SEC_IN_MONTH*6, SEC_IN_YEAR, SEC_IN_YEAR*5];

	// initialize datasets
	var pastTemps = [];
	var pastHumidities = [];
	var pastRain = [];
	var pastWind = [];

	// get past weather info
	for (var i = 0; i < dates.length; i++){

		var pastWeather = new XMLHttpRequest();
		pastWeather.open("GET", "https://api.darksky.net/forecast/5319a699f1d440da24185b3611ab1c89/" + lat + "," + lng + "," + (currentUTC-(dates[i])), false);
		pastWeather.send();

		var parsedPastWeather = JSON.parse(pastWeather.response);

		pastTemps.push(parsedPastWeather.currently.temperature);
		pastHumidities.push(Math.round(parsedPastWeather.currently.humidity * 100));
		pastRain.push(Math.round(parsedPastWeather.currently.precipProbability*100));
		pastWind.push(parsedPastWeather.currently.windSpeed);
	}

	// create historical weather data charts
	createCharts(historyLabels, pastTemps, pastRain, pastHumidities, pastWind);
}

function createCharts(historyLabels, pastTemps, pastRain, pastHumidities, pastWind){

	// get relevant chart
	var ctx = document.getElementById('tempChart');

	// add labels and data
	var data = {

		labels: historyLabels,
		datasets: [
			{
				label: "Temperature (F)",
				pointRadius: 1,
				borderJoinStyle: 'miter',
				data: pastTemps.reverse(),
				lineTension: 0.1,
				backgroundColor: 'rgba(117, 185, 190, 0.5)',
				borderColor: 'rgba(117, 185, 190, 1)'
			}
		]
	}

	// create chart
	var chart = new Chart(ctx, {
		type: 'line',
		data: data
	})


	ctx = document.getElementById('humidChart');
	data = {

		labels: historyLabels,
		datasets: [
			{
				label: "Humidity (%)",
				pointRadius: 1,
				borderJoinStyle: 'miter',
				data: pastHumidities.reverse(),
				lineTension: 0.1,
				backgroundColor: 'rgba(117, 185, 190, 0.5)',
				borderColor: 'rgba(117, 185, 190, 1)'
			}
		]
	}

	chart = new Chart(ctx, {
		type: 'line',
		data: data
	})

	ctx = document.getElementById('rainChart');
	data = {

		labels: historyLabels,
		datasets: [
			{
				label: "Chance of Rain (%)",
				pointRadius: 1,
				borderJoinStyle: 'miter',
				data: pastRain.reverse(),
				lineTension: 0.1,
				backgroundColor: 'rgba(117, 185, 190, 0.5)',
				borderColor: 'rgba(117, 185, 190, 1)'
			}
		]
	}

	chart = new Chart(ctx, {

		type: 'line',
		data: data
	})

	ctx = document.getElementById('windChart');
	data = {

		labels: historyLabels,
		datasets: [
			{
				label: "Wind Speed (MPH)",
				pointRadius: 1,
				borderJoinStyle: 'miter',
				data: pastWind.reverse(),
				lineTension: 0.1,
				backgroundColor: 'rgba(117, 185, 190, 0.5)',
				borderColor: 'rgba(117, 185, 190, 1)'
			}
		]
	}

	chart = new Chart(ctx, {

		type: 'line',
		data: data
	})
}